from django.db import models

# Create your models here.
class Formulario(models.Model):
    id = models.AutoField(primary_key = True)
    nombre = models.CharField('Nombre', max_length = 100, null = False, blank = False)
    edad = models.CharField('Edad', max_length = 100, null = False, blank = False)
    comentario = models.TextField('Coment', max_length = 100, null = False, blank = False)
    foto = models.ImageField('Fotografia', upload_to="mascotas", null = True, blank = True)


    class Meta:
       verbose_name = 'Formulario'
       verbose_name_plural = 'Formularios'

    def __str__(self):
        return self.nombre